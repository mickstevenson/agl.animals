﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AGL.Animals.Data;
using AGL.Animals.Shared.Model;

namespace AGL.Animals.BLL
{
    public class PersonDataProvider : IPersonDataProvider
    {
        public PersonDataProvider(IPersonRepository repo)
        {
            _repo = repo;
        }

        public async Task<Dictionary<Gender, IEnumerable<Animal>>> GetAnimalsGroupedByOwnerGender()
        {
            var people = await _repo.GetPeople();

            if (people == null || !people.Any())
                return new Dictionary<Gender, IEnumerable<Animal>>();

            var results = people.GroupBy(p => p.Gender)
                .Select(genderGroup =>
                    new
                    {
                        Gender = genderGroup.Key,
                        Animals = genderGroup.Where(person => person.Pets != null).SelectMany(person => person.Pets)
                    })
                .ToDictionary(genderGroup => genderGroup.Gender, genderGroup => genderGroup.Animals);

            return results;
        }

        public async Task<Dictionary<Gender, IEnumerable<string>>> GetAnimalNamesByOwnerGender(bool distinctNames = false)
        {
            var genderGrouping = await GetAnimalsGroupedByOwnerGender();

            Func<IEnumerable<Animal>, IEnumerable<string>> animalNames = (animals) =>
                distinctNames
                    ? animals.Select(animal => animal.Name).Distinct()
                    : animals.Select(animal => animal.Name);

            var results = genderGrouping.ToDictionary(genderGroup => genderGroup.Key,
                genderGroup => animalNames(genderGroup.Value).OrderBy(animalName => animalName).AsEnumerable());

            return results;
        }

        private readonly IPersonRepository _repo;
    }
}