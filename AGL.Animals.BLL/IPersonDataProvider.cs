﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AGL.Animals.Shared.Model;

namespace AGL.Animals.BLL
{
    public interface IPersonDataProvider
    {
        Task<Dictionary<Gender, IEnumerable<Animal>>> GetAnimalsGroupedByOwnerGender();
    }
}