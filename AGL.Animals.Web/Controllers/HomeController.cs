﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AGL.Animals.BLL;
using AGL.Animals.Web.Models;

namespace AGL.Animals.Web.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IPersonDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public async Task<ActionResult> Index()
        {
            var results = await _dataProvider.GetAnimalsGroupedByOwnerGender();

            var viewModel = new AnimalListViewModel
            {
                Records = results,
            };

            return View(viewModel);
        }

        private readonly IPersonDataProvider _dataProvider;
    }
}