﻿using System.Collections.Generic;
using AGL.Animals.Shared.Model;

namespace AGL.Animals.Web.Models
{
    public class AnimalListViewModel
    {
        public Dictionary<Gender, IEnumerable<Animal>> Records { get; set; }
    }
}