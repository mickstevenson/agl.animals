﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AGL.Animals.Shared.Model;

namespace AGL.Animals.Data
{
    public interface IPersonRepository
    {
        Task<IEnumerable<Person>> GetPeople();
    }
}