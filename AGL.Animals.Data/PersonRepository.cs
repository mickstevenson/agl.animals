﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AGL.Animals.Data.Converters;
using AGL.Animals.Shared.Model;
using Newtonsoft.Json;

namespace AGL.Animals.Data
{
    public class PersonRepository : IPersonRepository
    {
        private readonly Uri _baseApiUrl;
        private readonly string _sharedAssembleName;
        private readonly string _modelNamespace;

        public PersonRepository(Uri baseBaseApiUrl, string sharedAssembleName, string modelNamespace)
        {
            _baseApiUrl = baseBaseApiUrl;
            _sharedAssembleName = sharedAssembleName;
            _modelNamespace = modelNamespace;
        }

        public async Task<IEnumerable<Person>> GetPeople()
        {
            IEnumerable<Person> people = null;

            var client = GetClient();
            var response = await client.GetAsync(@"people.json");

            if (response.IsSuccessStatusCode)
            {
                people = await response.Content.ReadAsAsync<IEnumerable<Person>>(
                    new[]
                    {
                        new JsonMediaTypeFormatter
                        {
                            SerializerSettings = new JsonSerializerSettings
                            {
                                Converters = new List<JsonConverter>
                                {
                                    new AnimalJsonConverter(_sharedAssembleName, _modelNamespace)
                                }
                            }
                        }
                    });
            }

            return people;
        }

        private HttpClient GetClient()
        {
            var client = new HttpClient
            {
                BaseAddress = _baseApiUrl,
            };

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }        
    }
}