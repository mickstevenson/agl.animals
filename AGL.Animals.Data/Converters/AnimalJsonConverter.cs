﻿using System;
using System.IO;
using AGL.Animals.Shared.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AGL.Animals.Data.Converters
{
    public class AnimalJsonConverter : JsonConverter
    {
        private readonly string _sharedAssembleName;
        private readonly string _modelNamespace;
        private readonly IJObjectWrapper _jObjectWrapper;

        public AnimalJsonConverter(string sharedAssembleName, string modelNamespace, IJObjectWrapper jObjectWrapper = null)
        {
            _sharedAssembleName = sharedAssembleName;
            _modelNamespace = modelNamespace;
            _jObjectWrapper = jObjectWrapper ?? new JObjectWrapper();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = _jObjectWrapper.Load(reader);

            if (jObject["type"] == null || jObject["type"].ToString() == string.Empty)
                throw new InvalidDataException("no animal type provided");

            var typeName = $"{_modelNamespace}.{jObject["type"]}";

            Animal animal;

            try
            {
                var objHandle = Activator.CreateInstance(_sharedAssembleName, typeName);
                animal = (Animal)objHandle.Unwrap();
            }
            catch (TypeLoadException ex)
            {
                var message = $"unable to instantiate type: {typeName} in assembly: {_sharedAssembleName}";
                throw new InvalidDataException(message, ex);
            }

            animal.Name = jObject["name"].Value<string>();

            return animal;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(Animal) == objectType;
        }

        
    }

    public interface IJObjectWrapper
    {
        JObject Load(JsonReader reader);
    }

    public class JObjectWrapper : IJObjectWrapper
    {
        public JObject Load(JsonReader reader)
        {
            return JObject.Load(reader);
        }
    }
}