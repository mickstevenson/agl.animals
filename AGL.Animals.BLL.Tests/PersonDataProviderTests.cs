﻿using System.Linq;
using AGL.Animals.Data;
using AGL.Animals.Shared.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AGL.Animals.BLL.Tests
{
    [TestClass]
    public class PersonDataProviderTests
    {
        [TestMethod]
        public void PersonDataProvider_UnsupportedAnimal_ThrowsException()
        {
            var people = new []
            {
                new Person { Age = 35, Gender = Gender.Male, Name = "John Smith", Pets = new Animal[] { new Cat { Name = "Scar Face Claw"}, new Fish { Name = "Goldy"} }},
                new Person { Age = 19, Gender = Gender.Female, Name = "Jane Smith", Pets = new Animal[] { new Dog { Name = "Mr Woof"} }},
                new Person { Age = 18, Gender = Gender.Female, Name = "Peny Jones", Pets = new Animal[] { new Dog { Name = "Sparky"} }},
                new Person { Age = 13, Gender = Gender.Female, Name = "Claire Jones", Pets = new Animal[] { new Cat { Name = "Andy"} }}
            };

            var repo = new Mock<IPersonRepository>();
            repo.Setup(m => m.GetPeople()).ReturnsAsync(people);

            var personDataProvider = new PersonDataProvider(repo.Object);

            var task = personDataProvider.GetAnimalNamesByOwnerGender();
            task.Wait();

            var result = task.Result;

            Assert.IsTrue(result.Count == 2);

            Assert.IsTrue(result[Gender.Female].Count() == 3);
            Assert.IsTrue(result[Gender.Female].ToList()[0] == "Andy");
            Assert.IsTrue(result[Gender.Female].ToList()[1] == "Mr Woof");
            Assert.IsTrue(result[Gender.Female].ToList()[2] == "Sparky");

            Assert.IsTrue(result[Gender.Male].Count() == 2);
            Assert.IsTrue(result[Gender.Male].ToList()[0] == "Goldy");
            Assert.IsTrue(result[Gender.Male].ToList()[1] == "Scar Face Claw");
        }
    }
}