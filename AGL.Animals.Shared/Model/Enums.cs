﻿namespace AGL.Animals.Shared.Model
{
    public enum Gender
    {
        Unspecified = 0,
        Male,
        Female,
    }
}