﻿using System.Collections.Generic;

namespace AGL.Animals.Shared.Model
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }
        public IEnumerable<Animal> Pets { get; set; }
    }
}