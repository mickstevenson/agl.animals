﻿namespace AGL.Animals.Shared.Model
{
    public abstract class Animal
    {
        public string Name { get; set; }
    }
}