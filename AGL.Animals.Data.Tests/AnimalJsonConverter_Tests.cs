﻿using System.IO;
using AGL.Animals.Data.Converters;
using AGL.Animals.Shared.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AGL.Animals.Data.Tests
{
    [TestClass]
    public class AnimalJsonConverterTests
    {
        private const string sharedAssembleName = "AGL.Animals.Shared";
        private const string modelNamespace = "AGL.Animals.Shared.Model";

        [TestMethod]
        public void AnimalJsonConverter_UnsupportedAnimal_ThrowsException()
        {
            var jsonReader = new Mock<JsonReader>();
            var jsonSerializer = new Mock<JsonSerializer>();
            var jObjectWrapper = new Mock<IJObjectWrapper>();
            var jsonObject = new JObject { ["type"] = "Whale" };

            jObjectWrapper.Setup(m => m.Load(It.IsAny<JsonReader>())).Returns(jsonObject);

            var provider = new AnimalJsonConverter(sharedAssembleName, modelNamespace, jObjectWrapper.Object);

            Assert.ThrowsException<InvalidDataException>(() => provider.ReadJson(jsonReader.Object, typeof(Cat), null, jsonSerializer.Object));
        }

        [TestMethod]
        public void AnimalJsonConverter_NoAnimalType_ThrowsException()
        {
            var jsonReader = new Mock<JsonReader>();
            var jsonSerializer = new Mock<JsonSerializer>();
            var jObjectWrapper = new Mock<IJObjectWrapper>();
            var jsonObject = new JObject { ["type"] = "" };

            jObjectWrapper.Setup(m => m.Load(It.IsAny<JsonReader>())).Returns(jsonObject);

            var provider = new AnimalJsonConverter(sharedAssembleName, modelNamespace, jObjectWrapper.Object);

            Assert.ThrowsException<InvalidDataException>(() => provider.ReadJson(jsonReader.Object, typeof(Cat), null, jsonSerializer.Object));
        }

        [TestMethod]
        public void AnimalJsonConverter_NullAnimalType_ThrowsException()
        {
            var jsonReader = new Mock<JsonReader>();
            var jsonSerializer = new Mock<JsonSerializer>();
            var jObjectWrapper = new Mock<IJObjectWrapper>();
            var jsonObject = new JObject();

            jObjectWrapper.Setup(m => m.Load(It.IsAny<JsonReader>())).Returns(jsonObject);

            var provider = new AnimalJsonConverter(sharedAssembleName, modelNamespace, jObjectWrapper.Object);

            Assert.ThrowsException<InvalidDataException>(() => provider.ReadJson(jsonReader.Object, typeof(Cat), null, jsonSerializer.Object));
        }

        [TestMethod]
        public void AnimalJsonConverter_ValidAnimalTypeCat_ReturnsCat()
        {
            var catName = "Scar Face Claw";
            var jsonReader = new Mock<JsonReader>();
            var jsonSerializer = new Mock<JsonSerializer>();
            var jObjectWrapper = new Mock<IJObjectWrapper>();
            var jsonObject = new JObject { ["type"] = "Cat", ["name"] = catName};

            jObjectWrapper.Setup(m => m.Load(It.IsAny<JsonReader>())).Returns(jsonObject);

            var provider = new AnimalJsonConverter(sharedAssembleName, modelNamespace, jObjectWrapper.Object);
            var result = provider.ReadJson(jsonReader.Object, typeof(Cat), null, jsonSerializer.Object);
            Assert.IsTrue(result.GetType() == typeof(Cat));
            Assert.IsTrue(((Cat)result).Name == catName);
        }
    }
}